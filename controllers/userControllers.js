const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if the email exists

/*
			Steps: 
			1. Use mongoose "find" method to find duplicate emails
			2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (req, res) => {
  return User.find({ email: req.body.email })
    .then((result) => {
      //  The result of the find() method returns an array of objects.
      // we can use array.length() method for checking the current result length
      console.log(result);
      //  The user already exists
      if (result.length > 0) {
        return res.send("User already exists!");
      }
      //  There are no duplicate found.
      else {
        return res.send("No duplicate found!");
      }
    })
    .catch((err) => res.send(err));
};

// User registration

/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (req, res) => {
  let newUser = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    //  Syntax: bcrypt:hashSync(dataTobeEncrypted,salt)
    // salt - salt rounds that the bcrypt algorithm that will runt o encrypt the password.
    password: bcrypt.hashSync(req.body.password, 10),
    mobileNumber: req.body.mobileNumber,
  });

  console.log(newUser);
  return newUser
    .save()
    .then((user) => {
      console.log(user);
      res.send(true);
    })
    .catch((err) => {
      console.log(err);
      res.send(false);
    });
};

// rounds = 10 : 10 hashing/seconds

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not

   john1234 -> encrypt -> register
   encrypt(john1234) -> decrypt -> login

*/

module.exports.loginUser = (req, res) => {
  console.log(req.body);
  return User.findOne({ email: req.body.email }).then((result) => {
    //   User does not exists
    if (result == null) {
      return res.send({ message: "No user found!" });
    }
    //   User exists
    else {
      //  Syntax: compareSync(data, encrypted)
      const isPasswordCorrect = bcrypt.compareSync(
        req.body.password,
        result.password
      );
      // If the passwords match/result of the above code is true.

      // Generate an access token
      // Uses the "createAccessToken" method defined in the "auth.js" file
      // Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects

      if (isPasswordCorrect) {
        return res.send({
          accessToken: auth.createAccessToken(result),
        });
      } else {
        return res.send({ message: "Incorrect password" });
      }
    }
  });
};

// S38 Activity
/* 
   1. Create a "/details/:id" route that will accept the user's Id to retrieve the details of a user.
   2. Create a getProfile controller method for retrieving the details of the user:
      - Find the document in the database using the users's ID
      - Reassign the password of the returned document to an empty string ("")
      - Return the result back to the program
   3. Process a GET request at the details:id route using postman to retrieve the details of the user.
*/
// module.exports.getProfile = (req, res) => {
//   return User.findById(req.params.userId).then((result) => {
//     result.password = "";
//     res.send(result);
//   });
// };

module.exports.getProfile = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);

  return User.findById(userData.id).then((result) => {
    result.password = "***";
    res.send(result);
  });
  // return User.findById(req.params.userId).then((result) => {
  //   result.password = "";
  //   res.send(result);
  // });
};

// Enroll a Course

/*
 	Steps:
	// Users
	1. Find the document in the database using the user's ID (token)
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
	//Courses
	1. Find the document in the database using the course ID provided in the request body.
	2. Add the user ID to the course enrollees array.
	3. Update the document in the MongoDB Atlas Database
*/

// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user.

// User -> Enrollments Array
// Courses -> Enrollees Array
module.exports.enroll = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  let courseName = await Course.findById(req.body.courseId).then(
    (result) => result.name
  );

  let data = {
    // User ID and email will be retrieved from the request header.
    userId: userData.id,
    email: userData.email,
    // Course ID will be retrieved from the request body.
    courseId: req.body.courseId,
    courseName: courseName,
  };
  console.log(data);
  // user is updated if we receveived a "true" value
  let isUserUpdated = await User.findById(data.userId).then((user) => {
    user.enrollments.push({
      courseId: data.courseId,
      courseName: data.courseName,
    });
    // Save the updated user information in the database.
    return user
      .save()
      .then((result) => {
        console.log(result);
        return true;
      })
      .catch((err) => {
        console.log(err);
        return false;
      });
  });

  console.log(isUserUpdated);
  let isCourseUpdated = await Course.findById(data.courseId).then((course) => {
    course.enrollees.push({
      userId: data.userId,
      email: data.email,
    });
    if (course.slots <= 0) {
      course.isActive = false;
    } else {
      course.slots -= 1;

      return course
        .save()
        .then((result) => {
          console.log(result);
          return true;
        })
        .catch((err) => {
          console.log(err);
          return false;
        });
    }
  });

  console.log(isCourseUpdated);

  isUserUpdated && isCourseUpdated ? res.send(true) : res.send(false);
};
// Minus the slots available by 1
