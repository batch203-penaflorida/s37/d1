const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

// Route for Creating Course

router.post("/", auth.verify, courseControllers.addCourse);

// Route for viewing all courses (admin only)
router.get("/all", auth.verify, courseControllers.getAllCourses);

// Route for viewing all active courses (all users)
router.get("/", courseControllers.getAllActive);

// Route for viewing a specific course
router.get("/:courseId", courseControllers.getCourse);

// Route for update a specific course
router.put("/:courseId", auth.verify, courseControllers.updateCourse);

// Route for archiving specific course
router.patch(
  "/archive/:courseId",
  auth.verify,
  courseControllers.archiveCourse
);

module.exports = router;
