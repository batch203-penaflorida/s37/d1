const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

// Allows our backend application to be available with our front end application.
// Cross Origin Resource Sharing.
const app = express();

// Port

// MongoDB Connection
mongoose.connect(
  "mongodb+srv://admin:admin@zuitt-bootcamp.m9kt1ev.mongodb.net/batch203_bookingAPI?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the cloud database."));
// End of Mongo Db Connection

// This syntax will allow flexibility when using the application locally or as a hosted app.

// Start of middlewares
// Allow all resources to access our backend application

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Defines the '/users' string to be inlcuded for all user routes in the index.
app.use("/users", userRoutes);

// "/courses" string will be included for all course routes inside the courseRoutes.
app.use("/courses", courseRoutes);

// End of middlewares
const port = process.env.PORT || 4000;

app.listen(port, () => console.log(`API is now online on port ${port}.`));
